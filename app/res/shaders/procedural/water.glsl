#shader vertex
#version 450 core
layout(location = 0) in vec3 Pos;
layout(location = 1) in vec2 UVs;
out vec3 vPos;
out vec2 vUVs;
out vec3 vNorm;

void main() {
	vPos = Pos;
	vUVs = UVs;
	vNorm = vec3(0, 1, 0);
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader tessControl
#version 450 core
layout(vertices = 3) out;

in vec3 vPos[];
in vec2 vUVs[];
in vec3 vNorm[];
out vec3 tcPos[];
out vec2 tcUVs[];
out vec3 tcNorm[];
uniform vec3 _eye;

float LOD(float d1, float d2) {
	float avg = (d1 + d2) * 0.1;
	float valStep = 1.015;
	float minVal = 0.;
	float maxVal = 64. - minVal;
	return (ceil((maxVal + 1.) / pow(valStep, avg)) + minVal) - 1.;
}

void main() {
#define ID gl_InvocationID
	// Pass values
	tcPos[ID] = vPos[ID];
	tcUVs[ID] = vUVs[ID];
	tcNorm[ID] = vNorm[ID];
	// Write levels only once
	if (ID == 0) {
		// Dists
		float e0 = distance(_eye, vPos[0]);
		float e1 = distance(_eye, vPos[1]);
		float e2 = distance(_eye, vPos[2]);
		// Levels
		gl_TessLevelOuter[0] = LOD(e1, e2); // Edge BC
		gl_TessLevelOuter[1] = LOD(e2, e0); // Edge CA
		gl_TessLevelOuter[2] = LOD(e0, e1); // Edge AB
		gl_TessLevelInner[0] = (gl_TessLevelOuter[0] +
								gl_TessLevelOuter[1] +
								gl_TessLevelOuter[2]) / 3.;
	}
#undef ID
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader tessEval
#version 450 core
layout(triangles, fractional_even_spacing, ccw) in;

in vec3 tcPos[];
in vec2 tcUVs[];
in vec3 tcNorm[];
out vec3 tePos;
out vec2 teUVs;
out vec3 teNorm;

uniform int _time;
uniform float uRand;
uniform float uOctave;
uniform float uTimeFactor;
uniform float uNoiseFactor;

uniform vec3 _eye;
uniform float _yaw;
uniform float _deltaTime;

uniform mat4 matP;
uniform mat4 matV;
uniform mat4 matVP;

uniform sampler2D u_PerlinTex2Color;

void main() {

	tePos =
		gl_TessCoord.x * tcPos[0] +
		gl_TessCoord.y * tcPos[1] +
		gl_TessCoord.z * tcPos[2];

	teUVs =
		gl_TessCoord.x * tcUVs[0] +
		gl_TessCoord.y * tcUVs[1] +
		gl_TessCoord.z * tcUVs[2];

	teNorm =
		gl_TessCoord.x * tcNorm[0] +
		gl_TessCoord.y * tcNorm[1] +
		gl_TessCoord.z * tcNorm[2];

	vec2 offsetA = vec2(0.5, 1.0) * _time * uTimeFactor;
	vec2 offsetB = vec2(1.0, 0.5) * _time * uTimeFactor;
	float perlinA = texture(u_PerlinTex2Color, teUVs + offsetA).x;
	float perlinB = texture(u_PerlinTex2Color, teUVs + offsetB).x;
	float perlin = (perlinA + perlinB) * pow(uNoiseFactor, 1.8);
	tePos.y += perlin;

	float margin = 4.;
	float distPE = distance(tePos.xz, _eye.xz);
	bool camIsNear = distPE < margin;
	tePos.y = camIsNear ? tePos.y - mix(0, perlin, normalize(margin - distPE)) : tePos.y;

	gl_Position = matP * matV * vec4(tePos, 1.);
}




//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//

#shader geometry
#version 450 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 tePos[3];
in vec2 teUVs[3];
in vec3 teNorm[3];
out vec3 gPos;
out vec2 gUVs;
out vec3 gNorm;
out vec3 gWire;

void main() {

	gPos = tePos[0];
	gUVs = teUVs[0];
	gNorm = teNorm[0];
	gWire = vec3(1, 0, 0);
	gl_Position = gl_in[0].gl_Position;
	EmitVertex();

	gPos = tePos[1];
	gUVs = teUVs[1];
	gNorm = teNorm[1];
	gWire = vec3(0, 1, 0);
	gl_Position = gl_in[1].gl_Position;
	EmitVertex();

	gPos = tePos[2];
	gUVs = teUVs[2];
	gNorm = teNorm[2];
	gWire = vec3(0, 0, 1);
	gl_Position = gl_in[2].gl_Position;
	EmitVertex();

	EndPrimitive();
}

//
// ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ----- x ---
//


#shader fragment
#version 450 core

in vec3 gPos;
in vec2 gUVs;
in vec3 gNorm;
in vec3 gWire;
layout(location = 0) out vec3 fboPos;
layout(location = 1) out vec3 fboNorm;
layout(location = 2) out vec4 fboColor;

uniform sampler2D u_PerlinTex2Color;
uniform bool _wireFrame;

float amplify(float d, float scale, float offset) {
	d = scale * d + offset;
	d = clamp(d, 0, 1);
	d = 1 - exp2(-2 * d*d);
	return d;
}

void main() {
	fboPos = gPos;
	fboNorm = normalize(gNorm);

	// color gradient
	vec3 c1 = vec3(0.0, 0.9, 0.9);
	vec3 c2 = vec3(0.7, 0.1, 0.3);
	float perlin = texture(u_PerlinTex2Color, gUVs).r;
	vec3 mixedColor = mix(c2, c1, perlin);

	if (_wireFrame) {
		float wire = min(min(gWire.x, gWire.y), gWire.z);
		mixedColor *= amplify(wire, 40, -0.5);
	}

	fboColor = vec4(mixedColor, 5.); // <== roughness
}
