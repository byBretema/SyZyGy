#shader vertex
#version 450 core
out vec2 vTexCoord;
layout(location = 0) in vec3 planePos;
void main() {
	vTexCoord = vec2(0.5) + planePos.xy * 0.5;
	gl_Position = vec4(planePos, 1);
}


#shader fragment
#version 450 core

// Global vars

float Roughness;
vec3 vPos, N, V, Ka, Kd;


// Context data

in vec2 vTexCoord;

layout(location = 0) out vec4 glFragColor;

uniform mat4 matV; // To fix light position

uniform sampler2D fboPos;
uniform sampler2D fboNorm;
uniform sampler2D fboColor;


// Lights
struct LightData {
	vec4 position;
	vec4 color;
	vec3 attenuation;
	float angle;
	vec3 direction;
	float intensity;
};
layout(binding = 0) uniform _LIGHTS{
	float Ia;
	int nLights;
	LightData LIGHTS[100];
};
vec3 light(in LightData _light) {
	// Light data
	vec3 L = normalize((matV*_light.position).xyz - vPos);
	float LD = length(L); // For attenuation
	vec3 H = normalize(L + V); // For blinn phong
	float I = _light.intensity;
	vec3 C = _light.color.rgb;
	// Attenuation
	vec3 attF = _light.attenuation;
	float A = 1.0 / max((attF.x + attF.y*LD + attF.z*LD*LD), 1.0);
	// Object color
	vec3 diffuse = clamp(Kd * dot(N, L), 0, 1);
	vec3 specular = vec3(1.) * pow(max(dot(N, H), 0.0001), 1.);
	// Contribution of light to the object
	return I * C * A * (diffuse + specular);
}


// Entypoint

void main() {

	// Init globals
	vPos = texture(fboPos, vTexCoord).rgb;
	V = normalize(-vPos);
	N = texture(fboNorm, vTexCoord).rgb;
	Kd = texture(fboColor, vTexCoord).rgb;
	Roughness = texture(fboColor, vTexCoord).a; // More => Less specular shine

	// Shade
	vec3 shade = vec3(1.3, 1.8, 1.6) * Kd;
	for (int i = 0; i < nLights; i++) { shade += light(LIGHTS[i]); }

	// Fog
	float fogDensity = 0.002;
	vec3  fofboColor = vec3(0.25, 0.1, 0.25); // Clear color
	float dist = length((matV * vec4(vPos, 1)));
	float fogFactor = clamp(1. / exp(dist * fogDensity), 0., 1.);
	shade = mix(fofboColor, shade, fogFactor);

	// Vignette
	float innerRad = .3;
	float outterRad = .7;
	float vigIntensity = .7;
	vec3 vigColor = vec3(0.45);
	float lenRelPos = length(vTexCoord - 0.5);
	float vigOpacity = smoothstep(innerRad, outterRad, lenRelPos) * vigIntensity;
	shade = mix(shade, vigColor, vigOpacity);

	// Output
	glFragColor = vec4(shade, 1.0);
}
