#shader vertex
#version 450 core

out vec2 vTexCoord;
layout(location = 0) in vec3 planePos;

void main() {
	vTexCoord = vec2(0.5) + planePos.xy * 0.5;
	gl_Position = vec4(planePos, 1);
}


// --------------------------------------------------------------------- //


#shader fragment
#version 450 core

in vec2 vTexCoord;
layout(location = 0) out vec4 glFragColor;

uniform sampler2D u_PerlinTexColor;
uniform sampler2D u_PerlinTexNorm;
uniform sampler2D u_PerlinTexPos;
uniform sampler2D fboColor;

void main() {

	// Init globals
	vec3 pos = texture(u_PerlinTexPos, vTexCoord).rgb;
	vec3 norm = texture(u_PerlinTexNorm, vTexCoord).rgb;
	vec3 color = texture(fboColor, vTexCoord).rgb;

	// Output
	glFragColor = vec4(color, 1);
}
