#shader vertex
#version 450 core

layout(location = 0) in vec3 planePos;

out vec3 vPos;
out vec2 vUVs;

void main() {
	vPos = planePos;
	gl_Position = vec4(planePos, 1);
	vUVs = vec2(0.5) + planePos.xy * 0.5;
}


// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---


#shader fragment
#version 450 core

in vec3 vPos;
in vec2 vUVs;

layout(location = 0)out vec3 fboPos;
layout(location = 1)out vec3 fboNorm;
layout(location = 2)out vec4 fboColor;

float Rand2D(in vec2 st) {
	return fract(sin(dot(st.xy, vec2(12.9898, 78.233))));
}

float Perlin(in vec2 V) {
	vec2 i = floor(V);
	vec2 w = fract(V);
	// Four corners in 2D of a tile
	float p0 = Rand2D(i);
	float p1 = Rand2D(i + vec2(1.0, 0.0));
	float p2 = Rand2D(i + vec2(0.0, 1.0));
	float p3 = Rand2D(i + vec2(1.0, 1.0));
	// Quintic interpolation curve
	w = w * w*w*(w*(w*6. - 15.) + 10.);
	// Noise interpolation
	return mix(p0, p1, w.x) +
		(p2 - p0) * w.y * (1.0 - w.x) +
		(p3 - p1) * w.x * w.y;
}

void main() {

	float total = 0.;
	float frequency = 2.;
	float amplitude = 10.;
	float maxValue = 0.;
	int octaves = 12;
	for (int i = 0; i < octaves; i++) {
		total += Perlin(vUVs * frequency) * amplitude;
		maxValue += amplitude;
		amplitude *= 0.5;
		frequency *= 2.3;
	}
	total /= maxValue; // "/maxValue" normalize the noise.

	fboPos = vPos;
	fboColor = vec4(vec3(total), 1.0);
}
