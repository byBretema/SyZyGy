#shader compute
#version 440 core

// LAYOUTS

struct Particle {
	vec4 currPos;
	vec4 currVel;
	vec4 initPos;
	vec4 initVel;
	vec4 color;
};

layout(std430, binding = 1) buffer _particles { Particle Particles[]; };
layout(std430, binding = 2) buffer _attractors { vec4 Attractors[]; };

layout(local_size_x = 128, local_size_y = 1, local_size_z = 1) in;

// CONSTS

const float DT = 0.1;

// UNIFORMS

uniform float uSphere;
uniform float uBasicStep;
uniform float uMass;
uniform float uDamping;
uniform float uAttractorsIntensity;

uniform float _deltaTime;
uniform int nAttractors = 0;

// PARTICLE UPDATE

void updateParticle(uint gID) {
	Particle particle = Particles[gID];

	// Curr pos and vel

	vec3 v0 = particle.currVel.xyz;
	vec3 p0 = particle.currPos.xyz;
	vec3 v1, p1;

	// Attraction

	vec3 AG = vec3(0, 0, 0);
	for (int i = 0; i < nAttractors; i++) {
		vec3 distV = Attractors[i].xyz - p0;
		AG += normalize(distV) * uAttractorsIntensity;
	}

	// Step :: SemiImplicit

	float M = uMass;
	float D = uDamping;

	vec3 Gforce = AG * M;
	vec3 Dforce = -D * v0;
	vec3 F = Gforce + Dforce;

	v1 = ((M + DT * D)*v0 + DT * F) / (M + DT * D);
	p1 = p0 + v1 * DT;

	// Life and update

	float lifeTime = particle.currVel.w - 0.005;

	particle.currPos = vec4(p1, 1.);
	particle.currVel = vec4(v1, lifeTime);

	//if (lifeTime <= 0.) {
	//	particle.currPos = -particle.initPos;
	//}
	if (lifeTime <= 0.) {
		particle.currPos = particle.initPos;
		particle.currVel = particle.initVel;
	}

	// Color

	vec4 color = mix(
		vec4(1.0, 1.0, 0.0, 1.0), // Min life color :: azure
		vec4(0.0, 0.3, 0.5, 1.0), // Max life color :: orange
		(particle.currVel.w / particle.initVel.w) // Life percentage
	);
	particle.color = color;

	// Save

	Particles[gID] = particle;

}

// COMPUTE ENTRYPOINT

void main() {
	uint gID;
	uvec3 total = gl_NumWorkGroups * gl_WorkGroupSize;

	gID = gl_GlobalInvocationID.x;
	updateParticle(gID);
	gID = gl_GlobalInvocationID.y * total.x + gl_GlobalInvocationID.x;
	updateParticle(gID);
}
