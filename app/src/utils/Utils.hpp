/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once
#include <string>
#include <unordered_map>
#include <glm/glm.hpp>
#include <assimp/types.h>
#include "../ogl/Shader.hpp"

class Utils {
private:
	Utils() {}
	static int prevTime;
	static std::unordered_map<int, Shader*> shaders;
	static std::unordered_map<std::string, unsigned int> preProcessTextures;
public:
	static float deltaTime;

	/**
		@params Path of the config json file.
		@result Load all configurable variables from a json file.
	*/
	static void config(const std::string& path, const int& windowX, const int& windowY);

	/*
		@params Path of the image
		@params (W)idth to fill with the width of the image
		@params (H)eight to fill with the height of the image
		@result Image data
	*/
	static unsigned char* imgLoad(const char* path, unsigned int &w, unsigned int &h);

	/*
		@params filePath given from ASSIMP materials' system
		@result ID of texture on the current OpenGL context
	*/
	static unsigned int texLoad(const char *fileName);

	// Retrieve the dirname of a given path
	static std::string dirname(const std::string& dirpath);

	// Retrieve the filename of a given filepath
	static std::string basename(const std::string& filePath);

	// Convert ASSIMP matrices to GLM matrices
	static glm::mat4 ai2glm(const aiMatrix4x4 &aiMat);
};
