/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include <iostream>
#include <chrono>

// Json
#include <fstream>
#include <json.hpp>
using json = nlohmann::json;

// Imgs
#include <FreeImage.h>
#define _CRT_SECURE_DEPRECATE_MEMORY
#include <memory.h>

// OpenGL :: For textures loader
//#include <GL/glew.h>

// glMaths
#include <glm/glm.hpp>
#define GLM_FORCE_RADIANS
#include <glm/gtc/matrix_transform.hpp>

// Own
#include "Utils.hpp"
#include "../3d/Model.hpp"
#include "../3d/Scene.hpp"
#include "../env/CameraManager.hpp"
#include "../env/LightManager.hpp"
#include "../io/Keyboard.hpp"
#include "../ogl/Error.hpp"
#include "../ogl/ParticleSystem.hpp"
#include "../ogl/FBO.hpp"
#include "../ogl/Shader.hpp"
#include "../ogl/Grid.hpp"

float Utils::deltaTime = 0;
std::unordered_map<int, Shader *> Utils::shaders;
std::unordered_map<std::string, unsigned int> Utils::preProcessTextures;

void Utils::config(const std::string &path, const int& winW, const int& winH) {
	std::ifstream configData(path);

	try {
		json jConfig = json::parse(configData);

		/* LIGHTS */
		for (json &j : jConfig.at("Lights")) {
			glm::vec4 position = glm::vec4(j.at("Position")[0],
										   j.at("Position")[1],
										   j.at("Position")[2],
										   j.at("Position")[3]);
			float intensity = j.at("Intensity");
			glm::vec4 color = glm::vec4(j.at("Color")[0],
										j.at("Color")[1],
										j.at("Color")[2],
										j.at("Color")[3]);
			glm::vec3 attenuation = glm::vec3(j.at("Attenuation")[0],
											  j.at("Attenuation")[1],
											  j.at("Attenuation")[2]);
			float angle = 0.f;
			if (j.count("Angle") > 0) {
				angle = j.at("Angle");
			}
			glm::vec3 direction(0.f);
			if (j.count("Direction") > 0) {
				direction = glm::vec3(j.at("Direction")[0],
									  j.at("Direction")[1],
									  j.at("Direction")[2]);
			}
			Light light(position, intensity, color, attenuation, angle, direction);
			LightManager::addLight(light);
			LightManager::lightsToUBO();
		}

		/* PREPROCESSING */
		if (jConfig.count("PreProcess") > 0)
			for (json &j : jConfig.at("PreProcess")) {
				// Json parse
				int ID = j.at("ID");
				std::string name = j.at("Name");
				std::string shaderPath = j.at("Shader");
				// Objs creation
				Shader* s = new Shader(shaderPath);
				FBO p = FBO(winW - 8, winH - 8, s);
				// Compute textures
				p.bind();
				p.draw();
				// Rebind screen
				glBindFramebuffer(GL_FRAMEBUFFER, 0);
				// Extract textures
				auto posTex = std::make_pair(name + "Pos", p.getPosTex());
				auto normTex = std::make_pair(name + "Norm", p.getNormTex());
				auto colorTex = std::make_pair(name + "Color", p.getColorTex());
				auto depthTex = std::make_pair(name + "Depth", p.getDepthTex());
				// Store textures
				preProcessTextures.insert(posTex);
				preProcessTextures.insert(normTex);
				preProcessTextures.insert(colorTex);
				preProcessTextures.insert(depthTex);
			}

		/* CAMERAS */
		if (jConfig.count("Cameras") > 0) {
			CameraManager::clear();
			for (json &j : jConfig.at("Cameras")) {
				float zoom = 60.f;
				// 0) Type conversion
				glm::vec3 pos = glm::vec3(j.at("Position")[0],
										  j.at("Position")[1],
										  j.at("Position")[2]);
				if (j.count("Zoom") > 0) {
					zoom = j.at("Zoom");
				}
				std::string mode = j.at("Mode");
				// 1) Add to the CameraManager
				Camera *c = new Camera(mode, zoom, pos);
				CameraManager::addCamera(c);
				CameraManager::setActive(0);
			}
		}

		/* SHADERS */
		for (json &j : jConfig.at("Shaders")) {
			// 0) Type conversion
			int id = j.at("ID");
			std::string path = j.at("Path");
			// 1) Create shader object
			Shader *s = new Shader(path);
			// 2) Pass preprocess textures
			s->preProcessTextures = preProcessTextures;
			// 3) Load shader uniforms
			for (unsigned int i = 0; i < j.at("Uniforms").size(); i++) {
				json u = j.at("Uniforms")[i];

				if (u.at("Type") == "Rand") {
					std::string name = u.at("Name");
					std::uniform_real_distribution<float> distr(1.f, 100.f);
					std::mt19937 mt(static_cast<unsigned int>(
						std::chrono::high_resolution_clock::now()
						.time_since_epoch().count())
					);
					s->variables[name] = distr(mt);
				}

				else if (u.at("Type") == "Texture") {
					std::string name = u.at("Name");
					std::string path = u.at("Path");
					s->textures[name] = Utils::texLoad(path.data());
				}

				else if (u.at("Type") == "Bool") {
					// Json parse
					std::string key = u.at("Key");
					std::string name = u.at("Name");
					bool initValue = u.at("InitValue");
					float iValue = (initValue) ? 1.f : 0.f;
					// To the shader
					s->variables[name] = iValue;
					// To the keyboard
					Keyboard::keys.insert({ key,
										  std::make_tuple(s,
														  name,
														  "Toggle",
														  0.f) });
				}

				else {
					// Json parse
					std::string keyDEC = u.at("Key-");
					std::string keyINC = u.at("Key+");
					std::string name = u.at("Name");
					float initValue = u.at("InitValue");
					float steps = (u.count("Steps") > 0) ? static_cast<float>(u.at("Steps")) : 0.1f;
					// To the shader
					s->variables[name] = initValue;
					// To the keyboard
					Keyboard::keys.insert({ keyDEC,
										  std::make_tuple(s,
														  name,
														  "Dec",
														  steps) });
					Keyboard::keys.insert({ keyINC,
										  std::make_tuple(s,
														  name,
														  "Inc",
														  steps) });
				}
			}
			// 4) Store shader for models
			shaders[id] = s;
		}

		/* GRID */
		if (jConfig.count("Grid") > 0)
			for (json &j : jConfig.at("Grid")) {
				// 0) Type conversion
				int renderID = j.at("RenderShader");
				int xSize = j.at("xSize");
				int zSize = j.at("zSize");
				int scale = j.at("Scale");
				int baseHeight = j.at("BaseHeight");
				// 1) Create the grid
				Grid g(shaders[renderID], xSize, zSize, scale, baseHeight);
				Scene::addGrid(g);
			}

			/* PARTICLE SYSTEM */
		if (jConfig.count("ParticleSystem") > 0)
			for (json &j : jConfig.at("ParticleSystem")) {
				// 0) Type conversion
				int renderID = j.at("RenderShader");
				int computeID = j.at("ComputeShader");
				int maxLifeTime = j.at("MaxLifeTime");
				glm::vec4 emitPos = glm::vec4(j.at("EmitPos")[0],
											  j.at("EmitPos")[1],
											  j.at("EmitPos")[2],
											  1.f);
				std::vector<glm::vec4> attractors;
				for (unsigned int i = 0; i < j.at("Attractors").size(); i++) {
					json cAttr = j.at("Attractors")[i];
					attractors.push_back(glm::vec4(cAttr[0],
												   cAttr[1],
												   cAttr[2],
												   1.f));
				}
				float distribution = j.at("Distribution");
				int particlesNum = j.at("ParticlesNum");
				int csWorkers = j.at("CsWorkers");
				// 1) Create particle system
				ParticleSystem ps(shaders[renderID],
								  shaders[computeID],
								  maxLifeTime,
								  emitPos,
								  attractors,
								  distribution,
								  particlesNum,
								  csWorkers);
				Scene::addParticleSystem(ps);
			}

			/* MODELS */
			/* TO-DO :: Allow texture overwrite */
		if (jConfig.count("Models") > 0)
			for (json &j : jConfig.at("Models")) {
				// 0) Type conversion
				std::string file = j.at("File");
				float rotX = j.at("Rotation")[0];
				float rotY = j.at("Rotation")[1];
				float rotZ = j.at("Rotation")[2];
				glm::vec3 scale = glm::vec3(j.at("Scale")[0],
											j.at("Scale")[1],
											j.at("Scale")[2]);
				glm::vec3 translation = glm::vec3(j.at("Translation")[0],
												  j.at("Translation")[1],
												  j.at("Translation")[2]);
				int shaderID = j.at("Shader");
				// 1) Load 3D file
				Model m = Model(file);
				// 2) Operate on its model matrix
				glm::mat4x4 model = m.getModelMat();
				// --- 2.2) Translation
				model = glm::translate(model, translation);
				// --- 2.1) Rotation order (z, x, y)
				model = glm::rotate(model, rotX, glm::vec3(1, 0, 0));
				model = glm::rotate(model, rotY, glm::vec3(0, 1, 0));
				model = glm::rotate(model, rotZ, glm::vec3(0, 0, 1));
				// --- 2.3) Scale
				model = glm::scale(model, scale);
				// --- 2.4) Set model matrix again
				m.setModelMat(model);
				// 3) Set shader to the model
				m.setShader(shaders[shaderID]);
				// 4) Add to scene
				Scene::addModel(m);
			}

			/* POSTPROCESSING */
		if (jConfig.count("PostProcess") > 0)
			for (json &j : jConfig.at("PostProcess")) {
				std::string shaderPath = j.at("Shader");
				// 1) Create the shader program
				Shader *s = new Shader(shaderPath);

				// 3) Load shader uniforms
				for (unsigned int i = 0; i < j.at("Uniforms").size(); i++) {
					json u = j.at("Uniforms")[i];

					if (u.at("Type") == "Rand") {
						std::string name = u.at("Name");
						std::uniform_real_distribution<float> distr(1.f, 100.f);
						std::mt19937 mt(static_cast<unsigned int>(
							std::chrono::high_resolution_clock::now()
							.time_since_epoch().count())
						);
						s->variables[name] = distr(mt);
					}

					else if (u.at("Type") == "Texture") {
						std::string name = u.at("Name");
						std::string path = u.at("Path");
						s->textures[name] = Utils::texLoad(path.data());
					}

					else if (u.at("Type") == "Bool") {
						// Json parse
						std::string key = u.at("Key");
						std::string name = u.at("Name");
						bool initValue = u.at("InitValue");
						float iValue = (initValue) ? 1.f : 0.f;
						// To the shader
						s->variables[name] = iValue;
						// To the keyboard
						Keyboard::keys.insert({ key,
											  std::make_tuple(s,
															  name,
															  "Toggle",
															  0.f) });
					}

					else {
						// Json parse
						std::string keyDEC = u.at("Key-");
						std::string keyINC = u.at("Key+");
						std::string name = u.at("Name");
						float initValue = u.at("InitValue");
						float steps = (u.count("Steps") > 0) ? static_cast<float>(u.at("Steps")) : 0.1f;
						// To the shader
						s->variables[name] = initValue;
						// To the keyboard
						Keyboard::keys.insert({ keyDEC,
											  std::make_tuple(s,
															  name,
															  "Dec",
															  steps) });
						Keyboard::keys.insert({ keyINC,
											  std::make_tuple(s,
															  name,
															  "Inc",
															  steps) });
					}
				}

				// 2) Pass preprocess textures
				s->preProcessTextures = preProcessTextures;
				// 3) Create postprocess fbo and add to the scene
				FBO p = FBO(winW, winH, s);
				Scene::addPostProcess(p);
			}
	}
	catch (json::exception &e) {
		std::cerr << "JSON (" << e.id << "):: " << e.what() << std::endl;
	}
}

unsigned char *Utils::imgLoad(const char *fileName, unsigned int &w, unsigned int &h) {
	// 1) Awake freeImage
	FreeImage_Initialise(true);
	//std::cout << "TEXTURE LOCATION: " << fileName << std::endl;

	// 2) Check image type compatibility
	FREE_IMAGE_FORMAT format = FreeImage_GetFileType(fileName, 0);
	if (format == FIF_UNKNOWN)
		format = FreeImage_GetFIFFromFilename(fileName);
	if ((format == FIF_UNKNOWN) || !FreeImage_FIFSupportsReading(format)) {
		//std::cerr << "IMG_LOAD :: Warning, incompatible image" << std::endl;
	}

	// 3) Check data integrity
	FIBITMAP *img = FreeImage_Load(format, fileName);
	if (img == nullptr) {
		//std::cerr << "IMG_LOAD :: Warning, image it's null" << std::endl;
	}

	// 4) Ensure of image's 32 bits
	img = FreeImage_ConvertTo32Bits(img);

	// 5) Fill (W)idth and (H)eight data
	w = FreeImage_GetWidth(img);
	h = FreeImage_GetHeight(img);

	// 6) BGRA to RGBA
	unsigned char *map = new unsigned char[4 * w * h];
	char *buff = (char *)FreeImage_GetBits(img);
	for (unsigned int j = 0; j < w * h; j++) {
		map[j * 4 + 0] = buff[j * 4 + 2];
		map[j * 4 + 1] = buff[j * 4 + 1];
		map[j * 4 + 2] = buff[j * 4 + 0];
		map[j * 4 + 3] = buff[j * 4 + 3];
	}

	// 7) UnBind
	FreeImage_Unload(img);
	FreeImage_DeInitialise();

	// 8) Return image data
	return map;
}

unsigned int Utils::texLoad(const char *fileName) {

	// 1) Load and Check the texture image
	unsigned int w, h;
	unsigned char *map = imgLoad(fileName, w, h);
	if (!map) {
		std::cerr << "TEXTURE_LOAD :: Error loading texture " << fileName << std::endl;
	}

	// 2) Create the texture on OpenGL
	unsigned int texId;
	GL_ASSERT(glGenTextures(1, &texId));
	GL_ASSERT(glActiveTexture(GL_TEXTURE0 + texId));
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, texId));
	GL_ASSERT(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, (GLvoid *)map));

	// 3) Free the image pointer
	delete[] map;

	// 4) Configure the texture
	GL_ASSERT(glGenerateMipmap(GL_TEXTURE_2D));
	GL_ASSERT(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
	GL_ASSERT(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER));
	GL_ASSERT(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER));
	GL_ASSERT(glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR));

	// 5) UnBind
	GL_ASSERT(glBindTexture(GL_TEXTURE_2D, 0));

	// 6) Return the OpenGL identifier
	return texId;
}

std::string Utils::dirname(const std::string &dirPath) {
	std::string dirname = dirPath.substr(0, dirPath.find_last_of('\\'));
	return dirname.substr(0, dirname.find_last_of('/'));
}

std::string Utils::basename(const std::string &filePath) {
	std::string basename = filePath.substr(filePath.find_last_of('\\') + 1, filePath.length());
	return basename.substr(basename.find_last_of('/') + 1, basename.length());
}

glm::mat4 Utils::ai2glm(const aiMatrix4x4 &transform) {
	return {
		(float)transform.a1, (float)transform.b1, (float)transform.c1, (float)transform.d1,
		(float)transform.a2, (float)transform.b2, (float)transform.c2, (float)transform.d2,
		(float)transform.a3, (float)transform.b3, (float)transform.c3, (float)transform.d3,
		(float)transform.a4, (float)transform.b4, (float)transform.c4, (float)transform.d4 };
}
