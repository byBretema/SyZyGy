/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include "../ogl/Error.hpp"
#include "Camera.hpp"
Camera::Camera(const std::string& mode,
			   const float& fov,
			   const glm::vec3& pos
) :
	mode(mode),
	fov(glm::radians(fov)),
	pos(pos) {
	this->updateView();
	//this->rotateY(glm::radians(90.f));
}

void Camera::updateView() {

	// 1) Projection
	if (this->mode == "Perspective") {
		// UPDATE :: Proj matrix
		this->proj = glm::perspective(this->fov, this->aspectRatio, this->zNear, this->zFar);
	} else if (this->mode == "Orthogonal") {
		float oW = this->windowW * orthoZ;
		float oH = this->windowH * orthoZ;
		// UPDATE :: Proj matrix
		this->proj = glm::ortho(-oW, oW, -oH, oH, this->zNear, this->zFar);
	}

	// 2) Movement :: https://bit.ly/2pQRS8Q

	// New front
	glm::vec3 newFront;
	newFront.x = cos(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
	newFront.y = sin(glm::radians(this->pitch));
	newFront.z = sin(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));

	// Front
	this->front = glm::normalize(newFront);
	// Right
	this->right = glm::normalize(glm::cross(this->front, glm::vec3(0.f, 1.f, 0.f)));
	// Up
	this->up = glm::normalize(glm::cross(this->right, this->front));

	// UPDATE :: View matrix
	this->view = glm::lookAt(this->pos, this->pos + this->front, this->up);
}
