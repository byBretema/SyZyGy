/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include <iostream>
#include "CameraManager.hpp"

int CameraManager::camIndex = 0;
std::vector<Camera*> CameraManager::cameras = { new Camera("Perspective", 25.f) };
Camera* CameraManager::active = cameras[0];

void CameraManager::loop() {
	active->updateView();
}

void CameraManager::clear() {
	cameras.clear();
}

void CameraManager::addCamera(Camera* c) {
	cameras.push_back(c);
}

void CameraManager::setActive(int index) {
	camIndex = 0;
	active = cameras[camIndex];
}

void CameraManager::switchAtiveCamera() {
	std::cerr << "Cams: " << cameras.size() << std::endl;
	camIndex = (camIndex + 1 < (int)cameras.size()) ? camIndex + 1 : 0;
	active = cameras[camIndex];
}

void CameraManager::switchCameraMode() {
	if (active->getMode() == "Perspective") {
		active->setMode("Orthogonal");
	} else if (active->getMode() == "Orthogonal") {
		active->setMode("Perspective");
	}
}

void CameraManager::updateAspectRatio(int w, int h) {
	for (Camera* camera : cameras) { camera->setAspectRatio(w, h); }
}
