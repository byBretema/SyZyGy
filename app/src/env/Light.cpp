/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include "Light.hpp"

Light::Light(glm::vec4 position, float intensity, glm::vec4 color,
			 glm::vec3 attenuation, float angle, glm::vec3 direction) {
	this->position = position;
	this->intensity = intensity;
	this->color = color;
	this->attenuation = attenuation;
	this->angle = angle;
	this->direction = direction;
}

LightData Light::data() {
	return LightData{
		this->position,
		this->color,
		this->attenuation,
		this->angle,
		this->direction,
		this->intensity
		//0.f
	};
}
