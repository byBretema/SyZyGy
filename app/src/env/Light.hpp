/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once

#include <string>
#include <glm/glm.hpp>
#include "../ogl/Shader.hpp"

struct LightData {
	glm::vec4 position;
	glm::vec4 color;
	glm::vec3 attenuation;
	float angle;
	glm::vec3 direction;
	float intensity;
	//float padding; // Without this broke because won't be multiple of sizeof(vec4) !!!
};

class Light {
public:

	Light() {};

	// Parametrized constructor
	Light(glm::vec4 position, float intensity, glm::vec4 color,
		  glm::vec3 attenuation, float angle = 0.f,
		  glm::vec3 direction = glm::vec3(0.f));

	LightData data();

/// "PROPERTIES" --------------------------------------------------------- ///

	// Light type
private:
	std::string type = "point";
public:
	inline std::string getType() const { return this->type; }
	inline void setType(const std::string& lightType) { this->type = lightType; }

	// Position
private:
	glm::vec4 position = glm::vec4(0.f);
public:
	inline glm::vec4 getPosition() const { return this->position; }
	inline void setPosition(const glm::vec4& position) { this->position = position; }
	inline void setPosition(const float& x, const float& y, const float& z, const float& w) { this->position = glm::vec4(x, y, z, w); }

	// Color
private:
	glm::vec4 color = glm::vec4(1.f, 0.f, 0.f, 0.f);
public:
	inline glm::vec4 getColor() const { return this->color; }
	inline void setColor(const glm::vec4& color) { this->color = color; }
	inline void setColor(const float& r, const float& g, const float& b, const float& a) { this->color = glm::vec4(r, g, b, a); }

	// Intensity
private:
	float intensity;
public:
	inline float getIntensity() const { return this->intensity; }
	inline void setIntensity(const float& intensity) { this->intensity = intensity; }

	// Atenuation coefficients
private:
	glm::vec3 attenuation = glm::vec3(1.f, 2.f, 3.f);
public:
	inline glm::vec3 getAttenuation() const { return this->attenuation; }
	inline void setAttenuation(const glm::vec3& attenuation) { this->attenuation = attenuation; }
	inline void setAttenuation(const float& c1, const float& c2, const float& c3) { this->attenuation = glm::vec3(c1, c2, c3); }


	/* --- Only for Spot type --- */

	// Angle
private:
	float angle; // In radians
public:
	inline float getAngle() const { return this->angle; }
	inline void setAngle(const float& angle) { this->angle = angle; }

	// Direction
private:
	glm::vec3 direction = glm::vec3(0.f);
public:
	inline glm::vec3 getDirection() const { return this->direction; }
	inline void setDirection(const glm::vec3& direction) { this->direction = direction; }
	inline void setDirection(const float& x, const float& y, const float& z) { this->direction = glm::vec3(x, y, z); }

	/* END --- Only for Spot type --- */

};
