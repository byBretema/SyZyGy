#include "ParticleSystem.hpp"
#include <fstream>
#include <sstream>
#include "../utils/Utils.hpp"
#include <chrono>

ParticleSystem::ParticleSystem(
	Shader * renderShader,
	Shader * computeShader,
	int maxLifeTime,
	glm::vec4 emitPos,
	std::vector<glm::vec4> attractors,
	float distribution,
	int particlesNum,
	int csWorkers
) :
	computeShader(computeShader),
	renderShader(renderShader),
	maxLifeTime(maxLifeTime),
	emitPos(emitPos),
	attractors(attractors),
	distribution(distribution),
	particlesNum(particlesNum),
	csWorkers(csWorkers) {

	// Uniform num of attractors
	glProgramUniform1i(
		this->computeShader->getProgram(),
		this->computeShader->getUniformID("nAttractors"),
		this->attractors.size()
	);

	// Uniform emit point
	glProgramUniform4fv(
		this->computeShader->getProgram(),
		this->computeShader->getUniformID("emitPoint"),
		1,
		&this->emitPos[0]
	);

	// Uniform Distribution
	auto seed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
	this->mtNum = std::mt19937(static_cast<unsigned int>(seed));
	this->distr = Distribution(this->emitPos, this->distribution);
	std::uniform_real_distribution<float> lifeDistr(1.f, (float)this->maxLifeTime);

	// Create particles
	for (int i = 0; i < particlesNum; i++) {
		Particle p;

		float x = distr.x(mtNum);
		float y = distr.y(mtNum);
		float z = distr.z(mtNum);
		float lifeTime = lifeDistr(mtNum);

		p.initPos = glm::vec4(x, y, z, 1.f);
		p.currPos = p.initPos;

		p.initVel = glm::vec4(0.f, 0.f, 0.f, lifeTime);
		p.currVel = p.initVel;

		this->particles.push_back(p);
	}

	// VAO :: particle system
	GL_ASSERT(glGenVertexArrays(1, &this->VAO));
	GL_ASSERT(glBindVertexArray(this->VAO));

	// VBO :: particles
	unsigned int particleVBO;
	GL_ASSERT(glGenBuffers(1, &particleVBO));
	// SSBO
	GL_ASSERT(glBindBuffer(GL_SHADER_STORAGE_BUFFER, particleVBO));
	GL_ASSERT(glBufferData(GL_SHADER_STORAGE_BUFFER, this->particles.size() * sizeof(struct Particle), &this->particles[0], GL_DYNAMIC_DRAW));
	// SSBO :: binding = 1
	GL_ASSERT(glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, particleVBO));
	// ARRAY BUFFER
	GL_ASSERT(glBindBuffer(GL_ARRAY_BUFFER, particleVBO));
	// ARRAY BUFFER :: location = 0
	GL_ASSERT(glEnableVertexAttribArray(0));
	GL_ASSERT(glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Particle), (GLvoid*)offsetof(Particle, currPos)));
	// ARRAY BUFFER :: location = 1
	GL_ASSERT(glEnableVertexAttribArray(1));
	GL_ASSERT(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Particle), (GLvoid*)offsetof(Particle, color)));

	// VBO :: attractors
	unsigned int attractVBO;
	GL_ASSERT(glGenBuffers(1, &attractVBO));
	// SSBO
	GL_ASSERT(glBindBuffer(GL_SHADER_STORAGE_BUFFER, attractVBO));
	GL_ASSERT(glBufferData(GL_SHADER_STORAGE_BUFFER, this->attractors.size() * sizeof(glm::vec4), &this->attractors[0], GL_DYNAMIC_DRAW));
	// SSBO :: binding = 2
	GL_ASSERT(glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, attractVBO));

}

void ParticleSystem::draw() {

	glBindVertexArray(this->VAO);

	int workZ = 1;
	int workY = 1;
	int workX = (this->particlesNum / csWorkers) / workY / workZ;

	this->computeShader->use();
	GL_ASSERT(glDispatchCompute(workX, workY, workZ));
	GL_ASSERT(glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT));

	this->renderShader->use();
	GL_ASSERT(glDrawArrays(GL_POINTS, 0, this->particlesNum));
}
