#include "Grid.hpp"
#include <algorithm>

template<typename T>
inline T clamp(T x, T a, T b) {
	return std::min(std::max(a, x), b);
}

Grid::Grid(Shader * renderShader, int cols, int rows, int scale, int baseHeight) : renderShader(renderShader), cols(cols), rows(rows), scale(scale) {

	// 1) Grid creation
	float size = (rows*cols);
	float cell = size / ((float)scale - 1);
	for (int z = 0; z < rows; z++) {
		for (int x = 0; x < cols; x++) {
			float coordX = (x - ((cols) / 2));
			float coordZ = (z - ((rows) / 2));
			uvs.push_back(glm::vec2(x, z));
			vertices.push_back(glm::vec3(coordX*scale, baseHeight, coordZ*scale));
		}
	}

	// 2) Grid indexation
	// * This is ok for a grid if you wanna use tessellation
	// * Defining the two triangles of a quad you have all needed info
	// * As triangle without any magic from primitive assembler
	for (int z = 0; z < rows - 1; z++) {
		for (int x = 0; x < cols - 1; x++) {

			// Vars
			int x0 = x;
			int z0 = z * rows;
			int x1 = x + 1;
			int z1 = (z + 1) * rows;

			// Indices
			int i00 = x0 + z0;
			int i01 = x0 + z1;
			int i10 = x1 + z0;
			int i11 = x1 + z1;

			// First triangle
			this->indices.push_back(i00);
			this->indices.push_back(i01);
			this->indices.push_back(i10);

			// Second triangle
			this->indices.push_back(i11);
			this->indices.push_back(i10);
			this->indices.push_back(i01);
		}
	}

	// 3) VAO
	GL_ASSERT(glGenVertexArrays(1, &this->VAO));
	GL_ASSERT(glBindVertexArray(this->VAO));

	// 4) VBO
	unsigned int loc;
	GL_ASSERT(glGenBuffers(1, &this->VBO));
	GL_ASSERT(glBindBuffer(GL_ARRAY_BUFFER, this->VBO));

	loc = 0;
	GL_ASSERT(glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(glm::vec3), &this->vertices[0], GL_DYNAMIC_DRAW));
	GL_ASSERT(glEnableVertexAttribArray(loc));
	GL_ASSERT(glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0));

	loc = 1;
	unsigned int VBO2;
	GL_ASSERT(glGenBuffers(1, &VBO2));
	GL_ASSERT(glBindBuffer(GL_ARRAY_BUFFER, VBO2));
	GL_ASSERT(glBufferData(GL_ARRAY_BUFFER, this->uvs.size() * sizeof(glm::vec2), &this->uvs[0], GL_DYNAMIC_DRAW));
	GL_ASSERT(glEnableVertexAttribArray(loc));
	GL_ASSERT(glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0));

	// 5) EBO
	GL_ASSERT(glGenBuffers(1, &this->EBO));
	GL_ASSERT(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO));
	GL_ASSERT(glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->indices.size() * sizeof(unsigned int), &this->indices[0], GL_DYNAMIC_DRAW));

}

void Grid::draw() {
	this->renderShader->use();
	glBindVertexArray(this->VAO);             //   v------ Work with triangles, 4=Quad, 2=IsoLine
	GL_ASSERT(glPatchParameteri(GL_PATCH_VERTICES, 3));
	GL_ASSERT(glDrawElements(GL_PATCHES, this->indices.size(), GL_UNSIGNED_INT, 0));
}
