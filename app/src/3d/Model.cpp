/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include "Model.hpp"
#include "../utils/Utils.hpp"

Model::Model(const std::string& modelPath) {
	Assimp::Importer importer;

	// 1) Setting for "aiProcess_GenSmoothNormals"
	/* http://assimp.sourceforge.net/lib_html/postprocess_8h.html#a64795260b95f5a4b3f3dc1be4f52e410a6afb4fee42eca4482674859196cb8685 */
	importer.SetPropertyInteger("AI_CONFIG_PP_GSN_MAX_SMOOTHING_ANGLE", 80);

	// 2) Read file using configured "importer"
	/* http://assimp.sourceforge.net/lib_html/postprocess_8h.html#aaa612ed3f167a8999405d53fc309594b */
	const aiScene *scene = importer.ReadFile(modelPath, aiProcessPreset_TargetRealtime_Quality);

	// 3) Check for errors
	if (!scene ||
		!scene->mRootNode ||
		scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE) {
		// Send error to the Error stream
		this->modelError(importer.GetErrorString());
		return;
	}

	// 4) Store data of path
	this->modelPath = modelPath;
	this->dirPath = Utils::dirname(modelPath);

	// 5) Process ASSIMP's root node recursively
	this->nodeTreat(scene->mRootNode, scene);
}


void Model::nodeTreat(const aiNode* node, const aiScene* scene) {
	Node newNode;

	// 1) Calc the model matrix for this node (hierarchy matters)
	aiNode* parent = node->mParent;
	aiMatrix4x4 aiTransform = node->mTransformation;
	while (parent != nullptr) {
		aiTransform *= parent->mTransformation;
		parent = parent->mParent;
	}
	// 1.1) Needs to parse it to GLM before store
	glm::mat4 transform = Utils::ai2glm(aiTransform);
	newNode.transform = transform;

	// 2) Process node's meshes
	for (unsigned int i = 0; i < node->mNumMeshes; i++) {
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		newNode.meshes.push_back(this->meshTreat(mesh, scene));
	}

	// 3) Store the node on the model
	this->nodes.push_back(newNode);

	// 4) Recurisvity with childrens
	for (unsigned int i = 0; i < node->mNumChildren; i++) {
		this->nodeTreat(node->mChildren[i], scene);
	}
}


Mesh Model::meshTreat(const aiMesh* mesh, const aiScene* scene) {
	std::vector<Vertex> vertices;
	std::vector<Texture> textures;
	std::vector<unsigned int> indices;

	// 0) --- Chek mesh data
	if (!mesh->HasPositions()) {
		this->errPosition = true;
		this->modelError("No positions");
	}
	if (!mesh->HasNormals()) {
		this->errNormal = true;
		this->modelError("No normals");
	}
	if (!mesh->HasVertexColors(0)) {
		this->errColor = true;
		//this->modelError("No colors");
	}
	if (!mesh->HasTextureCoords(0)) {
		this->errTexCoord = true;
		this->modelError("No texture coords");
	}
	if (!mesh->HasTangentsAndBitangents()) {
		this->errTangent = true;
		this->modelError("No tangents and bitangents");
	}
	// END --- Chek mesh data


	// 1) --- Mesh VERTEX iteration
	for (unsigned int i = 0; i < mesh->mNumVertices; i++) {
		Vertex vertex;

		// 1.1) Positions
		if (!errPosition) {
			vertex.Position.x = mesh->mVertices[i].x;
			vertex.Position.y = mesh->mVertices[i].y;
			vertex.Position.z = mesh->mVertices[i].z;
		}

		// 1.2) Normals
		if (!errNormal) {
			vertex.Normal.x = mesh->mNormals[i].x;
			vertex.Normal.y = mesh->mNormals[i].y;
			vertex.Normal.z = mesh->mNormals[i].z;
		}

		// 1.3) Colors
		if (!errColor) {
			vertex.Color.r = mesh->mColors[i]->r;
			vertex.Color.g = mesh->mColors[i]->g;
			vertex.Color.b = mesh->mColors[i]->b;
			vertex.Color.a = mesh->mColors[i]->a;
		}

		// 1.4) Texture Coordinates
		if (!errTexCoord) {
			// We assume 1 texture coordinate per vertex.
			// But some models can carry up to 8.
			vertex.TexCoord.x = mesh->mTextureCoords[0][i].x;
			vertex.TexCoord.y = mesh->mTextureCoords[0][i].y;
		}

		// 1.5) Tangents and Bitangents
		if (!errTangent) {

			// 1.5.1) Tangents
			vertex.Tangent.x = mesh->mTangents[i].x;
			vertex.Tangent.y = mesh->mTangents[i].y;
			vertex.Tangent.z = mesh->mTangents[i].z;

			// 1.5.2) BiTangents
			vertex.BiTangent.x = mesh->mBitangents[i].x;
			vertex.BiTangent.y = mesh->mBitangents[i].y;
			vertex.BiTangent.z = mesh->mBitangents[i].z;
		}

		// 1.6) Sent this vertex to the mesh's vertex vector
		vertices.push_back(vertex);
	}
	// END --- Mesh VERTEX iteration

	// 2) --- Mesh INDEX iteration
	/* Work only with triangle primitive: https://stackoverflow.com/a/35389665/7901063 */
	bool bugFace = false;
	for (unsigned int i = 0; i < mesh->mNumFaces; i++) {
		aiFace face = mesh->mFaces[i];
		if (face.mNumIndices == 3) {
			for (unsigned int j = 0; j < face.mNumIndices; j++) {
				indices.push_back(face.mIndices[j]);
			}
		} else if (!bugFace) {
			bugFace = true;
		}
	}
	if (bugFace) { modelError("Some faces has more than 3 indices"); }
// END --- Mesh INDEX iteration

// 3) --- Mesh MATERIALS iteration
	if (mesh->mMaterialIndex >= 0) {
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		/* Assumption for sampler names in the shaders: tex|Type|N
		 * "tex"  :: Simply as texture prefix
		 * "Type" :: The first in capital letter: Diffuse, Specular, Emissive
		 * "N"    :: equential number ranging from 1 to MAX_SAMPLER_NUMBER
		*/
		std::vector<Texture> map;
		// 3.1) Ambient
		map = this->mat2tex(material, aiTextureType_AMBIENT, "texAmbient");
		textures.insert(textures.end(), map.begin(), map.end());
		// 3.2) Diffuse
		map = this->mat2tex(material, aiTextureType_DIFFUSE, "texDiffuse");
		textures.insert(textures.end(), map.begin(), map.end());
		// 3.3) Specular
		map = this->mat2tex(material, aiTextureType_SPECULAR, "texSpecular");
		textures.insert(textures.end(), map.begin(), map.end());
		// 3.4) Emissive
		map = this->mat2tex(material, aiTextureType_EMISSIVE, "texEmissive");
		textures.insert(textures.end(), map.begin(), map.end());
		// 3.5) Height
		map = this->mat2tex(material, aiTextureType_HEIGHT, "texHeight");
		textures.insert(textures.end(), map.begin(), map.end());
		// 3.6 Normals
		map = this->mat2tex(material, aiTextureType_NORMALS, "texNormals");
		textures.insert(textures.end(), map.begin(), map.end());
		// 3.7) Lightmap / Ambient occlusion
		map = this->mat2tex(material, aiTextureType_LIGHTMAP, "texLightmap");
		textures.insert(textures.end(), map.begin(), map.end());
	}
	// END --- Mesh MATERIALS iteration

	// 4) Return a mesh object created from the extracted mesh data
	return Mesh(indices, vertices, textures);
}


std::vector<Texture> Model::mat2tex(const aiMaterial* mat,
									const aiTextureType& type,
									const std::string& typeName) {
	std::vector<Texture> textures;

	// 1) Iter over different textures
	for (unsigned int i = 0; i < mat->GetTextureCount(type); i++) {
		bool repeated = false;

		// 1.1) Get texture from assimp
		aiString path;
		mat->GetTexture(type, i, &path);

	#ifdef WIN32
		std::string fullPath = this->dirPath + "\\" + Utils::basename(path.C_Str());
	#else
		std::string fullPath = this->dirPath + "/" + Utils::basename(path.C_Str());
	#endif

		// 1.2) Check that the texture isn't loaded if it's push it
		//for (unsigned int j = 0; j < this->loadedTexs.size(); j++) {
		//	if (this->loadedTexs[j].path == fullPath) {
		//		textures.push_back(loadedTexs[j]);
		//		repeated = true;
		//		break;
		//	}
		//}

		// 1.3) If not repeated create own texture
		if (!repeated) {
			Texture texture;
			texture.id = Utils::texLoad(fullPath.c_str());
			texture.type = typeName;
			texture.path = fullPath;
			textures.push_back(texture);
			this->loadedTexs.push_back(texture);
		}
	}

	// 2) Return the extracted textures
	return textures;
}

void Model::modelError(const char* error) {
	std::cerr << "ASSIMP :: " << this->modelPath << " :: " << error << std::endl;
}
