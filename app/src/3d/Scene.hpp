/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once

#include "Model.hpp"
#include "../env/CameraManager.hpp"
#include "../ogl/Grid.hpp"
#include "../ogl/FBO.hpp"
#include "../ogl/ParticleSystem.hpp"

class Scene {

public:

	// Draw a set of grids
	static void drawGrids(int drawOnlyID = -1);

	// Emit a set of particles
	static void drawParticles();

	// Call the draw funtion of models
	static void drawModels();

	// Draw without postprocessing
	static void computeScene();

	// Add a grid to the scene
	static void addGrid(const Grid& grid);

	// Add a particle system to the scene
	static void addParticleSystem(const ParticleSystem& cs);

	// Add a model to the scene
	static void addModel(const Model& model);

	// Add preprocess step to the scene
	static void addPreProcess(const FBO& preprocess);

	// Add postprocess step to the scene
	static void addPostProcess(const FBO& postprocess);

	// Fix aspect ratio on postprocess draw
	static void postprocessAspect(const int& w, const int& h);

private:

	// Static
	Scene();
	Scene(Scene &&);
	Scene(const Scene &);

	// Vars
	static std::vector<Grid> grids;
	static std::vector<Model> models;
	static std::vector<FBO> preProcesses;
	static std::vector<FBO> postProcesses;
	static std::vector<ParticleSystem> particleSystems;

};
