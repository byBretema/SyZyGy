/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#pragma once

#include <chrono>

class Display {

public:
	static bool paused;

	// Trigger this per frame
	static void frameDraw();

	// Trigger this on window re-scale
	static void reShape(int w, int h);

private:

	// Static
	Display();
	Display(Display &&);
	Display(const Display &);

	// Efficient way of do "Idle"
	static void reDraw(int value);

	// FPS
	static int frames;
	static int timeBase;

	// Time.deltaTime
	static std::chrono::steady_clock::time_point fpsBefore;
	static std::chrono::steady_clock::time_point deltaBefore;
};
