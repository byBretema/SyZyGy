/**
*
* SyZyGy - Align your 3D ideas.
* @author Daniel Camba Lamas <cambalamas@gmail.com>
*
* @section LICENSE
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
* General Public License for more details at
* https://www.gnu.org/copyleft/gpl.html
*
* @section DESCRIPTION
*
* ...
*
*/

#include <iostream>
#include <GL/glew.h>
#include "Keyboard.hpp"
#include "../ogl/Error.hpp"
#include "../env/CameraManager.hpp"
#include "Display.hpp"
#include <sstream>
#include "../utils/GData.hpp"

umMap Keyboard::keys;
bool Keyboard::wireframe = false;

void Keyboard::keyboardReply(unsigned char key, int x, int y) {
	//std::cout << "KEYBOARD key \"uchar\" [ " << key << " ] Pressed" << std::endl;
	//std::cout << "KEYBOARD key \"string\" [ " << ss.str() << " ] Pressed" << std::endl;

	std::stringstream ss;
	ss << key;

	auto r = keys.equal_range(ss.str());
	for_each(r.first, r.second, [](umMap::value_type& K) {
		auto shader = std::get<0>(K.second);
		auto varStr = std::get<1>(K.second);
		auto action = std::get<2>(K.second);
		auto jump = std::get<3>(K.second);
		shader->setUniform1f(varStr, action, jump);
	});

	switch (key) {
		case KEY_ASTERISK:
			GData::isPostprocessActive = !GData::isPostprocessActive;
			std::cout << "Postprocess = " << std::boolalpha
				<< GData::isPostprocessActive << std::endl;
			break;

		//case KEY_PLUS: GData::activeConfig++; break;
		//case KEY_MINUS: GData::activeConfig--; break;


		// Siwtch between Wireframe and Fill mode
		case KEY_CAPITAL_W:
			wireframe = !wireframe;
			GData::wireFrame = wireframe;
		//if (wireframe) {
		//	GL_ASSERT(glPolygonMode(GL_FRONT_AND_BACK, GL_LINE));
		//	std::cout << "\tWireframe ON" << std::endl;
		//} else {
		//	GL_ASSERT(glPolygonMode(GL_FRONT_AND_BACK, GL_FILL));
		//	std::cout << "\tWireframe OFF" << std::endl;
		//}
			break;

		// Switch between Paused and Play state
		case KEY_P:
		case KEY_CAPITAL_P:
			Display::paused = !Display::paused;
			if (Display::paused) {
				std::cout << "\tSimulation paused" << std::endl;
			} else {
				std::cout << "\tSimulation play" << std::endl;
			}
			break;

		// Camera :: Switch active camera
		case KEY_TAB: CameraManager::switchAtiveCamera(); break;
		// Camera :: Swith mode
		case KEY_CAPITAL_C: case KEY_C: CameraManager::switchCameraMode(); break;
		// Camera :: Movement
		case KEY_W: CameraManager::active->moveBack();   break;
		case KEY_S: CameraManager::active->moveFront();  break;
		case KEY_A: CameraManager::active->moveLeft();   break;
		case KEY_D: CameraManager::active->moveRight();  break;
		case KEY_U: CameraManager::active->moveDown(); break;
		case KEY_CAPITAL_U: CameraManager::active->moveUp(); break;
		// Camera :: Rotation
		case KEY_I: CameraManager::active->rotateX(0.01f); break;
		case KEY_K: CameraManager::active->rotateX(-0.01f); break;
		case KEY_J: CameraManager::active->rotateY(0.01f); break;
		case KEY_L: CameraManager::active->rotateY(-0.01f); break;

		// Get out of the app
		case KEY_ESCAPE: exit(2); break;

	}
}
