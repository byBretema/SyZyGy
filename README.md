# SyZyGy - Render Engine

![SZG_render_test](https://imgur.com/H8YXujJ.png)

## Funcionalidades implementadas

* Niebla.
* Defferred Shading.
* Sombreado de phong.
* Controles por teclado y ratón.
* Luz puntual, direccional y focal.
* Rendering en múltiples pasadas.
* Cámara ortogonal y primera persona.
* Arquitectura dividida en múltiples clases.
* Número de luces ilimitado utilizando UBO's.
* Implementado usando la versión 4.4 de OpenGL.
* Número de cámaras ilimitado. *(Tantas como se definan en el JSON)*
* *Idle state* eficiente usando  `glutTimerFunc(55, reDraw, 0);`  para invocar el redibujado.
* **Carga de datos de fichero:** ASSIMP para los modelos 3D y JSON para la configuración del motor e indicar archivos a cargar.

## Controles

* **Esc :** Para salir del entorno.

* **w, a, s, d :** Para desplazar la cámara.

* **Clic izquierdo + desplazamiento :** Para rotar la cámara.

* **W :** Para activar el modo wireframe.
    * *Sólo útil sin postprocesado*

* **p :** Para cambiar el tipo de camara.
    * *Ortogonal*
    * *Perspectiva*

* **c :** Para cambiar entre las cámaras disponibles.
    * *En la config por defecto, se definen dos cámaras una por defecto en perspectiva y otra en ortogonal*

## PostProcesado

En el main hay dos lineas que llevan a cabo la carga de la ejecución en base a un fichero de configuración JSON.

### Sin postprocesado

`Utils::config("res/config/config.json");`

Dado que el ejemplo de *postprocesado* es muy simple, este será el mejor modo de cargar para testear todo el tema de las luces.

### Con postprocesado

`	Utils::config("res/config/config_with_postprocessing.json");`

En este caso hay dos shaders *súper* simples, **el primero** `res/shaders/postpro1.glsl` recoge la geometría y la lleva un fbo cuyo postproceso recogerá sólo el rojo de la imagen. **el segundo** `res/shaders/postpro2.glsl` recoge el valor extraido de la pasada anterior y lo usa para multiplicar un vec3 de donde se calcula el color final que ya se renderiza sobre la pantalla dando lugar a la geometría pintada en un tono azulado.

## Otros comentarios

Los shaders están escritos en un único archivo **.glsl** que incluye el shader de vértices y de fragmentos. Se parsea el archivo y se extrae (utilizando los indicativos ***#shader***) el código de cada una para pasarselo por separado a la GPU en el momento de crear un objeto tipo *Shader* en la arquitectura utilizada para el motor.

Hay un problema conocido en la carga de archivos FBX de 3dsMax, en el que no se cargan bien las texturas, por lo demás la carga de modelos 3D usando ASSIMP importa modelos que contienen varias mallas en un sólo fichero *(Como es el caso de la configuración de ejemplo que que "circulo" con el cono en su interior, provienen de un único fichero)*. Además de leer de los ficheros directamente las texturas y aplicar sus matrices Model para que se vean exactamente igual que se deja en Blender.
